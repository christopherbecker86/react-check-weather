import React, { Component } from 'react';
import { getSearchLocation } from './../helpers/Api';

class SearchLocation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchQuery: "",
            results: [],
        };
    }
    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value,
        });

        this.getSearchFromApi(value);

    };
    getSearchFromApi = async (value) => {
        const { results } = await getSearchLocation(value);
        this.setState({
            results,
        });
    }
    selectLocation = ({result}) => {
        const { lat, lon } = result;
        this.props.getApiWeather(lat, lon);
        this.clearSearchQuery();
    }
    clearSearchQuery = () => {
        this.setState({
            searchQuery: "",
            results: [],
        });
    }
    render() {
        const { results, searchQuery } = this.state;
        return (
            <div className="search-location">
                <input type="text" placeholder="Search by city" name="searchQuery" value={searchQuery} onChange={this.handleChange} />
                <div className="suggest-box">
                    <ul>
                        { results && results.map((result, index) => (
                            <li key={index}>
                                <a href="#" onClick={() => this.selectLocation({result})}>{result.name} <strong>{result.iso2}</strong></a>
                            </li>
                        )) }
                        { searchQuery && <li><a href="#" onClick={this.clearSearchQuery} ><strong>Clear search</strong></a></li> }
                    </ul>
                </div>
            </div>
        );
    }
}

export default SearchLocation;