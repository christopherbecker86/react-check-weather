import React, { Component } from 'react';
import {
  getLoaction,
  getWeather,
} from './helpers/Api';

import { kToC } from './helpers/KelvinToCelcius';

import SearchLocation from './components/SearchLocation';

import './assets/App.css';

const detailedInfo = {
  maxWidth: '600px',
  margin: 'auto',
  display: 'flex',
  paddingTop: '1rem',
  color: '#777777',
};
const flexStyle = {
  flexGrow: '2',
};

const textCenter = {
  textAlign: 'center',
  width: '100%',
}
const iconUrl = `https://cdn2.iconfinder.com/data/icons/weather-and-forecast-free/32`;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    this.getApiLocation();
  }
  getApiLocation = async () => {
    const {
      lat,
      lon,
    } = await getLoaction();
    this.getApiWeather(lat, lon);
    this.setState({
      lat,
      lon,
    });
  };
  getApiWeather = async (lat, lon) => {
    const { main, name, weather } = await getWeather(lat, lon);
    this.setState({
      main,
      name,
      weather,
    });
  };
  skyPicker = (condition) => {
    switch (condition) {
      case "Clear":
        return `${iconUrl}/Weather_Weather_Forecast_Hot_Sun_Day-512.png`
      case "Clouds":
        return `${iconUrl}/Weather_Weather_Forecast_Sunny_Sun_Cloudy-512.png`
      case "Rain":
      case "Drizzle":
      case "Thunderstorm":
        return `${iconUrl}/Weather_Weather_Forecast_Cloud_Snowing_Cloud_Climate-512.png`
      default:
        return `${iconUrl}/Weather_Weather_Forecast_Hot_Sun_Day-512.png`
    }
  }
  render() {
    const { weather, main, name } = this.state;
    return (
      <div className="App">
        <SearchLocation getApiWeather={this.getApiWeather} />
        { weather && 
          <div style={textCenter} className="position-relative">
            <img src={ this.skyPicker(weather[0].main) } alt={weather[0].description} />
            <div className="info-box">
              <h3 className="bigger-h3" style={textCenter}>
                {name}
              </h3>
              <div className="current-temp">
                { kToC(main.temp) }°C
              </div>
              <div style={detailedInfo}>
                <div style={flexStyle}>
                  <strong>Humidity</strong> { main.humidity }%
                </div>
                <div style={flexStyle}>
                  <strong>Min</strong> { kToC(main.temp_min) }°C
                </div>
                <div style={flexStyle}>
                  <strong>Max</strong> { kToC(main.temp_max) }°C
                </div>
              </div>
            </div>
          </div>
        }
      </div>
    );
  }
}

export default App;
