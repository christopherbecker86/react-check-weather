import axios from "axios";

const keyID = '274433cc662cde695cd08960c71f75ca';

export const getLoaction = () => axios({
    method: 'get',
    url: `http://ip-api.com/json`,
})
.then((response) => {
    return response.data;
});

export const getSearchLocation = (searchQuery) => axios({
    method: 'get',
    url: `https://www.meteoblue.com/en/server/search/query3?query=${searchQuery}&itemsPerPage=5`,
})
.then((response) => {
    return response.data;
});

export const getWeather = (lat, lon) => axios({
    method: 'get',
    url: `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${keyID}`,
})
.then((response) => {
    return response.data;
});