export const kToC = (kelvin) => {
    return (kelvin - 273.15).toFixed();
}